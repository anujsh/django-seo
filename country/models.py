from django.db import models


# Create your models here.
from django.utils import timezone


class Country(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=50)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        verbose_name = 'country'
        verbose_name_plural = 'countries'
        db_table = 'countries'

    def __str__(self):
        return self.name
