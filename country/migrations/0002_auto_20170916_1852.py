# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-16 18:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('country', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='country',
            table='countries',
        ),
    ]
