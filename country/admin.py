from django.contrib import admin
from country.models import Country

# Register your models here.
from state.models import State


class StateInline(admin.TabularInline):
    model = State


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    inlines = [
        StateInline
    ]
