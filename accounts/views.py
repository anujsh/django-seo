from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

# Create your views here.
from django.contrib.auth.views import PasswordChangeView
from django.urls import reverse
from django.views.generic import TemplateView, UpdateView


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/profile.html'


class ChangePasswordView(LoginRequiredMixin, PasswordChangeView):
    model = User

    template_name = 'accounts/change_password.html'

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        messages.success(self.request, 'Password Successfully changed')
        return reverse('accounts:profile')


class UpdateProfile(LoginRequiredMixin, UpdateView):
    model = User
    fields = ['first_name', 'last_name']

    template_name = 'accounts/update.html'

    def get_object(self):
        return self.request.user

    def get_success_url(self):
        messages.success(self.request, 'Profile Updated Successfully')
        return reverse('accounts:profile')


class SettingView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/setting.html'

