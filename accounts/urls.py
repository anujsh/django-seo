from django.conf.urls import url

from . import views

app_name = 'accounts'
urlpatterns = [
    url(r'^$', views.ProfileView.as_view(), name='profile'),
    url(r'^profile/', views.ProfileView.as_view(), name='profile'),
    url(r'^change-password/', views.ChangePasswordView.as_view(), name='change_password'),
    url(r'^update/', views.UpdateProfile.as_view(), name='update'),
    url(r'^setting/', views.SettingView.as_view(), name='setting')
]
