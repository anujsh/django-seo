from django.db import models


# Create your models here.
from country.models import Country


class State(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=50)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        verbose_name = 'state'
        verbose_name_plural = 'states'
        db_table = 'states'

    def __str__(self):
        return self.name
