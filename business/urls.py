from django.conf.urls import url

from business.views3 import BusinessList, BusinessDetail, BusinessCreate, BusinessUpdate, BusinessDelete

app_name = 'business'
urlpatterns = [
    url(r'add/$', BusinessCreate.as_view(), name='add'),
    url(r'^$', BusinessList.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', BusinessDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/update$', BusinessUpdate.as_view(), name='update'),
    url(r'^(?P<pk>[0-9]+)/delete$', BusinessDelete.as_view(), name='delete'),
]
