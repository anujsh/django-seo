from django.contrib.auth.models import User
from django.db import models
from state.models import State


# Create your models here.

class BusinessType(models.Model):
    title = models.CharField(max_length=100)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        db_table = 'business_types'

    def __str__(self):
        return self.title


class Business(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    business_type = models.ForeignKey(BusinessType, on_delete=models.CASCADE)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        verbose_name = 'business'
        verbose_name_plural = 'businesses'
        db_table = 'businesses'

    def __str__(self):
        return self.name


class BusinessAddress(models.Model):
    business = models.OneToOneField(Business, on_delete=models.CASCADE)
    line_1 = models.CharField(max_length=200)
    line_2 = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.ForeignKey(State, on_delete=models.PROTECT)
    postal_code = models.CharField(max_length=15)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        verbose_name = 'business address'
        verbose_name_plural = 'business addresses'
        db_table = 'business_addresses'

    def __str__(self):
        return '%s, %s, %s, %s' % (self.line_1, self.line_2, self.city, self.state)


class BusinessEmail(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE)
    email = models.EmailField(unique=True, default=None)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        db_table = 'business_emails'

    def __str__(self):
        return self.email


class BusinessPhone(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=15, default=None)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        db_table = 'business_phones'

    def __str__(self):
        return self.phone_number


class BusinessWebsite(models.Model):
    business = models.ForeignKey(Business, on_delete=models.CASCADE)
    website = models.URLField(max_length=100, default=None)
    created = models.DateTimeField('date created', auto_now_add=True)
    modified = models.DateTimeField('last modified', auto_now=True)

    class Meta:
        db_table = 'business_websites'

    def __str__(self):
        return self.url
