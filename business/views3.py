from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction

# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from business.Forms import BusinessAddressFormSet, BusinessEmailFormSet, BusinessPhoneFormSet, BusinessWebsiteFormSet, \
    BusinessForm
from business.models import Business


class BusinessList(ListView):
    model = Business
    context_object_name = 'list_of_user_businesses'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessList, self).dispatch(*args, **kwargs)


class BusinessDetail(DetailView):
    context_object_name = 'business'
    queryset = Business.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessDetail, self).dispatch(*args, **kwargs)


class BusinessCreate(LoginRequiredMixin, CreateView):
    model = Business
    form_class = BusinessForm

    def form_valid(self, form):
        messages.success(self.request, 'form is valid')
        form.instance.user = self.request.user
        form.save()

    def get_success_url(self):
        messages.success(self.request, 'Business Added Successfully')
        return reverse('business:list')


class BusinessUpdate(UpdateView):
    model = Business

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessUpdate, self).dispatch(*args, **kwargs)


class BusinessDelete(DeleteView):
    model = Business

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessDelete, self).dispatch(*args, **kwargs)
