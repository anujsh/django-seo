from django.forms import ModelForm, inlineformset_factory

from business.models import Business, BusinessAddress, BusinessEmail, BusinessPhone, BusinessWebsite

"""
Form classes
"""


class BusinessForm(ModelForm):
    class Meta:
        model = Business
        exclude = ['user']


class BusinessAddressForm(ModelForm):
    class Meta:
        model = BusinessAddress
        exclude = ()


class BusinessEmailForm(ModelForm):
    class Meta:
        model = BusinessEmail
        exclude = ()


class BusinessPhoneForm(ModelForm):
    class Meta:
        model = BusinessPhone
        exclude = ()


class BusinessWebsiteForm(ModelForm):
    class Meta:
        model = BusinessWebsite
        exclude = ()


"""
inlineformset_factory
https://docs.djangoproject.com/en/1.11/topics/forms/modelforms/#inline-formsets
"""
BusinessAddressFormSet = inlineformset_factory(
    Business,
    BusinessAddress,
    form=BusinessAddressForm,
    extra=1,
    can_delete=False
)

BusinessEmailFormSet = inlineformset_factory(
    Business,
    BusinessEmail,
    form=BusinessEmailForm,
    extra=1,
    can_delete=False
)

BusinessPhoneFormSet = inlineformset_factory(
    Business,
    BusinessPhone,
    form=BusinessPhoneForm,
    extra=1,
    can_delete=False
)

BusinessWebsiteFormSet = inlineformset_factory(
    Business,
    BusinessWebsite,
    form=BusinessWebsiteForm,
    extra=1,
    can_delete=False
)
