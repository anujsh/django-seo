from django.contrib import admin

# Register your models here.
from business.models import Business, BusinessType, BusinessAddress, BusinessEmail, BusinessPhone, BusinessWebsite


class BusinessAddressInline(admin.TabularInline):
    model = BusinessAddress


class BusinessEmailInline(admin.TabularInline):
    model = BusinessEmail


class BusinessPhoneInline(admin.TabularInline):
    model = BusinessPhone


class BusinessInline(admin.TabularInline):
    model = Business


@admin.register(Business)
class BusinessAdmin(admin.ModelAdmin):
    inlines = [
        BusinessAddressInline,
        BusinessEmailInline,
        BusinessPhoneInline
    ]


@admin.register(BusinessType)
class BusinessTypeAdmin(admin.ModelAdmin):
    inlines = [
        BusinessInline
    ]

