from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction

# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from business.Forms import BusinessAddressFormSet, BusinessEmailFormSet, BusinessPhoneFormSet, BusinessWebsiteFormSet
from business.models import Business


class BusinessList(ListView):
    model = Business
    context_object_name = 'list_of_user_businesses'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessList, self).dispatch(*args, **kwargs)


class BusinessDetail(DetailView):
    context_object_name = 'business'
    queryset = Business.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessDetail, self).dispatch(*args, **kwargs)


class BusinessCreate(CreateView):
    model = Business
    fields = '__all__'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessCreate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(BusinessCreate, self).get_context_data(**kwargs)

        if self.request.POST:
            data['business_address'] = BusinessAddressFormSet(self.request.POST)
            data['business_email'] = BusinessEmailFormSet(self.request.POST)
            data['business_phone'] = BusinessPhoneFormSet(self.request.POST)
            data['business_website'] = BusinessWebsiteFormSet(self.request.POST)
        else:
            data['business_address'] = BusinessAddressFormSet()
            data['business_email'] = BusinessEmailFormSet()
            data['business_phone'] = BusinessPhoneFormSet()
            data['business_website'] = BusinessWebsiteFormSet()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        form.instance.user = self.request.user
        business_address = context['business_address']
        business_email = context['business_email']
        business_phone = context['business_phone']
        business_website = context['business_website']
        with transaction.atomic():
            self.object = form.save()

            if business_address.is_valid():
                messages.success(self.request, 'Business address is valid')
                business_address.save()

            if business_email.is_valid():
                messages.success(self.request, 'Email is valid')
                business_email.save()

            if business_phone.is_valid():
                messages.success(self.request, 'Phone is valid')
                business_phone.save()
            else:
                messages.success(self.request, 'Phone is valid')

            if business_website.is_valid():
                messages.success(self.request, 'Website is valid')
                business_website.save()

        return super(BusinessCreate, self).form_valid(form)

    def get_success_url(self):
        messages.success(self.request, 'Business Added Successfully')
        return reverse('business:list')


class BusinessUpdate(UpdateView):
    model = Business

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessUpdate, self).dispatch(*args, **kwargs)


class BusinessDelete(DeleteView):
    model = Business

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BusinessDelete, self).dispatch(*args, **kwargs)
